using System.Collections.Generic;
using System.Security.Claims;
using System.Threading.Tasks;
using Infoboat.Modules.News.Controllers;
using Infoboat.Modules.News.Data.DTO;
using Infoboat.Modules.News.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Moq;
using Xunit;

namespace Infoboat.Tests.NewsTests
{
    public class NewsControllerTests
    {
        private readonly Mock<INewsService> _mockNewsService;
        private readonly ControllerContext _controllerContext;
        public NewsControllerTests()
        {
            // Common Arrange for Every Test
            _mockNewsService = new Mock<INewsService>();

            // Override this step if special setup is required for a test
            _mockNewsService.Setup( mockNewsService =>
                        mockNewsService.GetNews(
                            It.IsAny<string>(),
                            It.IsAny<int>(),
                            It.IsAny<int>(),
                            It.IsAny<string>()
                        )
                    )
                .ReturnsAsync( new List<NewsDTO>() );

            ClaimsPrincipal user = new ClaimsPrincipal( 
                new ClaimsIdentity( 
                    new Claim[]
                    { 
                        new Claim( "username", "username" )
                    }
                )
            );

            _controllerContext = new ControllerContext();
            _controllerContext.HttpContext = new DefaultHttpContext { User = user };
        }

        [Fact]
        public async Task GetNews_PageNumberLessThanOne_ReturnsUnprocessableEntity()
        {
            //Arrange
            NewsController newsController = new NewsController( _mockNewsService.Object );
            newsController.ControllerContext = _controllerContext;

            //Act
            var result = await newsController.GetNews( sortBy: "latest", pageNumber: 0, pageSize: 1 );

            //Assert
            Assert.IsType<UnprocessableEntityResult>( result.Result );
        }

        [Fact]
        public async Task GetNews_PageSizeLessThanOne_ReturnsUnprocessableEntity()
        {
            //Arrange
            NewsController newsController = new NewsController( _mockNewsService.Object );
            newsController.ControllerContext = _controllerContext;

            //Act
            var result = await newsController.GetNews( sortBy: "latest", pageNumber: 1, pageSize: 0 );

            //Assert
            Assert.IsType<UnprocessableEntityResult>( result.Result );
        }

        [Fact]
        public async Task GetNews_NoNewsInRepository_ReturnsNotFound()
        {
            //Arrange
            _mockNewsService.Setup( mockNewsService =>
                        mockNewsService.GetNews(
                            It.IsAny<string>(),
                            It.IsAny<int>(),
                            It.IsAny<int>(),
                            It.IsAny<string>()
                        )
                    )
                .ReturnsAsync( (List<NewsDTO>)null );
            NewsController newsController = new NewsController( _mockNewsService.Object );
            newsController.ControllerContext = _controllerContext;

            //Act
            var result = await newsController.GetNews( sortBy: "latest", pageNumber: 1, pageSize: 20 );

            //Assert
            Assert.IsType<NotFoundResult>( result.Result );

        }

        [Fact]
        public async Task GetNews_NormalCase_ReturnsOk()
        {
            //Arrange
            NewsController newsController = new NewsController( _mockNewsService.Object );
            newsController.ControllerContext = _controllerContext;

            //Act
            var result = await newsController.GetNews( sortBy: "latest", pageNumber: 1, pageSize: 20 );

            //Assert
            Assert.NotNull( result.Result );
            Assert.IsType<OkObjectResult>( result.Result );
        }

    }
}