using System.Collections.Generic;
using System;
using Infoboat.Modules.Bookmarks.Data.Repositories;
using Infoboat.Modules.News.Data.Repositories;
using Moq;
using Xunit;
using Infoboat.Modules.Bookmarks.Models;
using Infoboat.Modules.News.Data.Models;
using Infoboat.Modules.News.Services;
using Infoboat.Modules.News.Data.DTO;
using System.Threading.Tasks;
using Infoboat.Modules.Bookmarks.Data.DTO;
using System.Linq;

namespace Infoboat.Tests.NewsTests
{
    public class NewsServiceTests
    {
        Mock<INewsRepository> _mockNewsRepository;
        Mock<IBookmarkRepository> _mockBookmarkRepository;
        public NewsServiceTests()
        {
            List<Bookmark> bookmarkInMemoryDatabase =  new List<Bookmark>
            {
                new Bookmark { BookmarkId = 1,
                               NewsItem = new NewsItem {
                                   NewsItemId=1,
                                   Title="News Title",
                                   Url="Test Url",
                                   ImgUrl="Test Image Url",
                                   Description = "Test Description",
                                   Source="Test Source" ,
                                   PublishedTimestamp = DateTime.Now
                                },
                               UserName = "username"
                             }                
            };

            List<NewsItem> newsInMemoryDatabase = new List<NewsItem>
            {
                new NewsItem { NewsItemId=1,
                               Title="News Title",
                               Url="Test Url",
                               ImgUrl="Test Image Url",
                               Description = "Test Description",
                               Source="Test Source",
                               PublishedTimestamp = new DateTime(2015, 12, 31, 5, 10, 20)
                            },
                new NewsItem { NewsItemId=2,
                               Title="News Title",
                               Url="Test Url",
                               ImgUrl="Test Image Url",
                               Description = "Test Description",
                               Source="Test Source",
                               PublishedTimestamp = new DateTime(2016, 12, 31, 5, 10, 20)
                            },
                new NewsItem { NewsItemId=3,
                               Title="News Title",
                               Url="Test Url",
                               ImgUrl="Test Image Url",
                               Description = "Test Description",
                               Source="Test Source",
                               PublishedTimestamp = new DateTime(2014, 12, 31, 5, 10, 20)
                            }
            };

            // Common Arrange for Every Test
            _mockNewsRepository = new Mock<INewsRepository>();
            _mockBookmarkRepository = new Mock<IBookmarkRepository>();

            // Override next step if special setup is needed for a test
            _mockNewsRepository.Setup( mockNewsRepository =>
                                       mockNewsRepository.GetNewsPageSortedByLatest(
                                           It.IsAny<int>(),
                                           It.IsAny<int>()
                                        )
                                    )
                                .ReturnsAsync( newsInMemoryDatabase );
            
            _mockBookmarkRepository.Setup( mockNewsRepository =>
                                           mockNewsRepository.GetUserBookmarks(
                                               It.Is<string>( username => username == "username" )
                                            )
                                    )
                                .ReturnsAsync( bookmarkInMemoryDatabase );
        }

        [Fact]
        public async Task GetNews_NoNewsInDatabase_ReturnsNull()
        {
            //Arrange
            _mockNewsRepository.Setup( mockNewsRepository =>
                                       mockNewsRepository.GetNewsPageSortedByLatest(
                                           It.IsAny<int>(),
                                           It.IsAny<int>()
                                        )
                                    )
                                .ReturnsAsync( new List<NewsItem>() );
            INewsService newsService = new NewsService( _mockNewsRepository.Object, _mockBookmarkRepository.Object );

            //Act
            var news = await newsService.GetNews( sortBy: "latest", pageNumber: 1, pageSize: 20, userName:"" );

            //Assert
            Assert.Null( news );
        }

        [Fact]
        public async Task GetNews_NormalCase_ReturnsListOfBookmarkDTO()
        {
            //Arrange
            INewsService newsService = new NewsService( _mockNewsRepository.Object, _mockBookmarkRepository.Object );

            //Act
            var news = await newsService.GetNews( sortBy: "latest", pageNumber: 1, pageSize: 20, userName:"" );

            //Assert
            Assert.IsType<List<NewsDTO>>( news );
            Assert.NotEmpty( news );
        }
        
        [Fact]
        public async Task GetNews_SortByRandomString_ReturnsNull()
        {
            //Arrange
            INewsService newsService = new NewsService( _mockNewsRepository.Object, _mockBookmarkRepository.Object );

            //Act
            var news = await newsService.GetNews( sortBy: "randomstring", pageNumber: 1, pageSize: 2, userName:"" );

            //Assert
            Assert.Null( news );
        }

        [Fact]
        public async Task GetNews_NoUser_IsBookmarkShouldBeFalseForEveryItemInReturnedList()
        {
            //Arrange
            INewsService newsService = new NewsService( _mockNewsRepository.Object, _mockBookmarkRepository.Object );

            //Act
            var newsList = await newsService.GetNews( sortBy: "latest", pageNumber: 1, pageSize: 2, userName:"" );

            //Assert
            Assert.NotEmpty( newsList );
            foreach ( var newsItem in newsList )
            {
                Assert.False( newsItem.IsBookmarked );                
            }
        }

        [Fact]
        public async Task GetNews_UserWithNoBookmarks_IsBookmarkShouldBeFalseForEveryItemInReturnedList()
        {
            //Arrange
            INewsService newsService = new NewsService( _mockNewsRepository.Object, _mockBookmarkRepository.Object );

            //Act
            var newsList = await newsService.GetNews( sortBy: "latest", pageNumber: 1, pageSize: 2, userName:"username_nobookmark" );

            //Assert
            Assert.NotEmpty( newsList );
            foreach ( var newsItem in newsList )
            {
                Assert.False( newsItem.IsBookmarked );                
            }
        }

        [Fact]
        public async Task GetNews_UserWithBookmarks_IsBookmarkShouldBeTrueForBookmarkedItemInReturnedList()
        {
            //Arrange
            INewsService newsService = new NewsService( _mockNewsRepository.Object, _mockBookmarkRepository.Object );

            //Act
            var newsEnumerable = await newsService.GetNews( sortBy: "latest", pageNumber: 1, pageSize: 2, userName:"username" );

            //Assert
            var newsList = Assert.IsType<List<NewsDTO>>( newsEnumerable );
            Assert.NotEmpty( newsList );
            Assert.True( newsList.ElementAt( 0 ).IsBookmarked );
            Assert.False( newsList.ElementAt( 1 ).IsBookmarked );
            Assert.False( newsList.ElementAt( 2 ).IsBookmarked );
        }
    }
}
