using System.Threading;
using System.Threading.Tasks;
using Infoboat.Modules.Bookmarks.Models;
using Infoboat.Modules.News.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace Infoboat.Global
{
    public interface IDataContext
    {
        DbSet<NewsItem> NewsCollection { get; set; }
        DbSet<Bookmark> Bookmarks { get; set; }
        Task<int> SaveChangesAsync(CancellationToken cancellationToken = default);
         
    }
}