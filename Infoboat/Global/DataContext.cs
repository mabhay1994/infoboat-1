using Infoboat.Modules.Bookmarks.Models;
using Infoboat.Modules.News.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace Infoboat.Global
{
    public class DataContext : DbContext, IDataContext
    {
        public DataContext(DbContextOptions<DataContext> options) : base(options)
        {
            Database.EnsureCreated();
        }
        public DbSet<NewsItem> NewsCollection { get; set; }
        public DbSet<Bookmark> Bookmarks { get; set; }
    }
}