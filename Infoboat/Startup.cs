using System;
using System.Collections.Generic;
using System.Net;
using Infoboat.Global;
using Infoboat.Modules.Bookmarks.Data.Repositories;
using Infoboat.Modules.Bookmarks.Services;
using Infoboat.Modules.News.Data.Repositories;
using Infoboat.Modules.News.Services;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;

namespace Infoboat
{
    public class Startup
    {//DDAAAAAA
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
// comment by abhay mendiratta -------------------------------------------		
            services.AddDbContext<DataContext>(options => options.UseNpgsql(
                String.Format(
                    Configuration.GetConnectionString("DefaultConnection"),
                    Environment.GetEnvironmentVariable("DATABASE_PASSWORD")
                ))
            );
            services.AddScoped<IDataContext>(provider => provider.GetService<DataContext>());
            services.AddScoped<INewsRepository, NewsRepository>();
            services.AddScoped<IBookmarkRepository, BookmarkRepository>();
            services.AddScoped<INewsService, NewsService>();
            services.AddScoped<IBookmarkService, BookmarkService>();
            services.AddControllers();
            services.AddAuthentication( options => 
                                        {
                                            options.DefaultAuthenticateScheme =  JwtBearerDefaults.AuthenticationScheme;
                                            options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                                        })
                    .AddJwtBearer(options =>
                    {
                        options.Authority = Environment.GetEnvironmentVariable("COGNITO_USER_POOL_URL");
                        options.TokenValidationParameters = new TokenValidationParameters
                        {
                            IssuerSigningKeyResolver = (s, securityToken, identifier, parameters) =>
                            {
                                var json = new WebClient().DownloadString(parameters.ValidIssuer + "/.well-known/jwks.json");
                                var keys = new JsonWebKeySet(json).Keys;
                                return (IEnumerable<SecurityKey>)keys;
                            },
                            ValidIssuer = Environment.GetEnvironmentVariable("COGNITO_USER_POOL_URL"),
                            ValidateIssuerSigningKey = true,
                            ValidateIssuer = true,
                            ValidateLifetime = true,
                            LifetimeValidator = (before, expires, token, param) => expires > DateTime.UtcNow,
                            ValidateAudience = false
                        };
                    });
            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new OpenApiInfo { Title = "Infoboat", Version = "v1" });
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.------
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseSwagger();
                app.UseSwaggerUI(c => c.SwaggerEndpoint("/swagger/v1/swagger.json", "Infoboat v1"));
            }

            // app.UseHttpsRedirection();aa
            app.UseAuthentication();

            app.UseRouting();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });
        }
    }
}
