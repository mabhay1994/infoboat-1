using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Infoboat.Modules.Bookmarks.Data.DTO;

namespace Infoboat.Modules.Bookmarks.Services
{
    public interface IBookmarkService
    {
        Task<IEnumerable<BookmarkDTO>> GetUserBookmarks( string userId );
        Task DeleteBookmarkAsync( int bookmarkId );
        Task<Tuple<int, BookmarkDTO>> SaveBookmark( int newsItemId, string userId );

    }
}