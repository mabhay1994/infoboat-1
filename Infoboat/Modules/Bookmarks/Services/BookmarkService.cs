using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Infoboat.Modules.Bookmarks.Data.DTO;
using Infoboat.Modules.Bookmarks.Data.Repositories;
using Infoboat.Modules.Bookmarks.Models;
using Infoboat.Modules.News.Data.Models;
using Infoboat.Modules.News.Data.Repositories;
namespace Infoboat.Modules.Bookmarks.Services
{
    public class BookmarkService : IBookmarkService
    {
        private readonly IBookmarkRepository _bookmarkRepository;
        private readonly INewsRepository _newsRespository;

        public BookmarkService(IBookmarkRepository bookmarkRepository, INewsRepository newsRespository)
        {
            _bookmarkRepository = bookmarkRepository;
            _newsRespository = newsRespository;
        }
        public async Task<Tuple<int, BookmarkDTO>> SaveBookmark(int newsItemId, string userName)
        {
            Bookmark existingBookmark = await _bookmarkRepository.GetBookmark(newsItemId, userName);
            NewsItem newsItem = await _newsRespository.GetNewsItem( newsItemId );
            if (existingBookmark != null) return Tuple.Create<int, BookmarkDTO>(409, null); ;
            if (newsItem ==  null) return Tuple.Create<int, BookmarkDTO>(404, null);
            Bookmark bookmark = new Bookmark
            {
                NewsItem = newsItem,
                UserName = userName
            };
            await _bookmarkRepository.SaveBookmark( bookmark );
            return Tuple.Create<int, BookmarkDTO>( 201, new BookmarkDTO(bookmark));
        }

        public async Task<IEnumerable<BookmarkDTO>> GetUserBookmarks(string userId)
        {
            IEnumerable<Bookmark> userBookmarks = await _bookmarkRepository.GetUserBookmarks(userId);

            if (userBookmarks == null) return null;

            List<BookmarkDTO> bookmarkDTOs = new List<BookmarkDTO>();
            foreach (Bookmark bookmark in userBookmarks)
            {
                bookmarkDTOs.Add(new BookmarkDTO(bookmark));
            }
            return bookmarkDTOs;
        }
        public async Task DeleteBookmarkAsync(int bookmarkId)
        {
            await _bookmarkRepository.DeleteBookmark(bookmarkId);
        }

    }
}