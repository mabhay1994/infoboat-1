using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Infoboat.Modules.Bookmarks.Data.DTO;
using Infoboat.Modules.Bookmarks.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Infoboat.Modules.Bookmarks.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    public class BookmarkController : ControllerBase
    {
        private readonly IBookmarkService _bookmarkService;

        public BookmarkController(IBookmarkService bookmarkService)
        {
            _bookmarkService = bookmarkService;
        }
        [HttpPost]
        public async Task<ActionResult> SaveBookmark( [FromBody] int newsItemId  )
        {
            if ( newsItemId < 1 ) return UnprocessableEntity();
            string userId = HttpContext.User.FindFirstValue("username");
            Tuple<int, BookmarkDTO> response = await _bookmarkService.SaveBookmark( newsItemId, userId );
            if ( response.Item1 == 409 )
            {
                return Conflict();                
            }
            else if ( response.Item1 == 404 )
            {
                return NotFound();
            }
            else
            {
                return Created("", response.Item2);
            }
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<BookmarkDTO>>> GetUserBookmarks()
        {
            string userId = HttpContext.User.FindFirstValue("username");
            var bookmarks = await _bookmarkService.GetUserBookmarks( userId );
            if (bookmarks == null)
            {
                return NoContent();
            }
            return Ok(bookmarks);
        }
        [HttpDelete]
        public ActionResult DeleteBookmark( int bookmarkId )
        {
            _bookmarkService.DeleteBookmarkAsync( bookmarkId );
            return NoContent();
        }
    }
}
