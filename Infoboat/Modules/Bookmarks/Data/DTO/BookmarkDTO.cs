using System;
using Infoboat.Modules.Bookmarks.Models;
using Infoboat.Modules.News.Data.Models;

namespace Infoboat.Modules.Bookmarks.Data.DTO
{
    public class BookmarkDTO
    {
        public BookmarkDTO(Bookmark bookmark)
        {
            this.BookmarkId = bookmark.BookmarkId;
            this.NewsItem = bookmark.NewsItem;
            this.DateCreated = bookmark.DateCreated;
            
        }
        public int BookmarkId { get; set; }

        public NewsItem NewsItem { get; set; }

        public DateTime DateCreated { get; set; }
    }
}