using System.Collections.Generic;
using System.Threading.Tasks;
using Infoboat.Modules.Bookmarks.Models;

namespace Infoboat.Modules.Bookmarks.Data.Repositories
{
    public interface IBookmarkRepository
    {
        Task<Bookmark> GetBookmark(int newsItemId, string userName);
        Task<IEnumerable<Bookmark>> GetUserBookmarks(string userName);
        Task SaveBookmark(Bookmark bookmark);
        Task DeleteBookmark(int bookmarkId);
    }
}