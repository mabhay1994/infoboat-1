using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Infoboat.Global;
using Infoboat.Modules.Bookmarks.Models;
using Microsoft.EntityFrameworkCore;

namespace Infoboat.Modules.Bookmarks.Data.Repositories
{
    public class BookmarkRepository : IBookmarkRepository
    {
        private readonly IDataContext _context;
        public BookmarkRepository(IDataContext context)
        {
            _context = context;
        }
        public async Task DeleteBookmark(int bookmarkId)
        {
            var bookmarkToDelete = await _context.Bookmarks.FindAsync( bookmarkId );
            if ( bookmarkToDelete == null )
            {
                return;
            }
            _context.Bookmarks.Remove(bookmarkToDelete);
            await _context.SaveChangesAsync();
        }

        public async Task<Bookmark> GetBookmark(int newsItemId, string userName)
        {
            Bookmark bookmark = await _context.Bookmarks
                                    .Include(b => b.NewsItem)
                                    .Where(b => b.UserName == userName
                                            && b.NewsItem.NewsItemId == newsItemId)
                                    .FirstOrDefaultAsync();
            return bookmark;
        }

        public async Task<IEnumerable<Bookmark>> GetUserBookmarks(string userName)
        {
            List<Bookmark> bookmarks = await _context.Bookmarks
                                .Include(bookmark => bookmark.NewsItem)
                                .Where(b => b.UserName == userName)
                                .ToListAsync();
            return bookmarks;
        }

        public async Task SaveBookmark(Bookmark bookmark)
        {
            _context.Bookmarks.Add(bookmark);
            await _context.SaveChangesAsync();
        }
    }
}