using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Infoboat.Modules.News.Data.Models;

namespace Infoboat.Modules.Bookmarks.Models
{
    public class Bookmark
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int BookmarkId { get; set; }
        [ForeignKey("NewsItemId")]
        public NewsItem NewsItem { get; set; }
        public string UserName { get; set; }

        public DateTime DateCreated { get; set; } = DateTime.Now;
    }
}