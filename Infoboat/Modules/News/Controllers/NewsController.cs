using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Infoboat.Modules.News.Data.DTO;
using Infoboat.Modules.News.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Infoboat.Modules.News.Controllers
{
    [Route("[controller]")]
    [ApiController]
    [Authorize]
    public class NewsController : ControllerBase
    {
        private readonly INewsService _newsService;

        public NewsController(INewsService newsService)
        {
            _newsService = newsService;
        }
        [HttpGet]
        [AllowAnonymous]
        public async Task<ActionResult<IEnumerable<NewsDTO>>> GetNews(string sortBy = "latest", int pageNumber = 1, int pageSize = 20)
        {
            string userId = HttpContext.User.FindFirstValue("username");
            if ( pageNumber < 1 || pageSize < 1 )
            {
                return UnprocessableEntity();
            }
            IEnumerable<NewsDTO> newsItems = await _newsService.GetNews( sortBy, pageNumber, pageSize, userId );
            if ( newsItems == null )
            {
                return NotFound();
            }
            return Ok( newsItems );
        }
        

    }
}
