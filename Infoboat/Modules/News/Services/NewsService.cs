using System.Collections.Generic;
using System.Threading.Tasks;
using Infoboat.Modules.Bookmarks.Data.Repositories;
using Infoboat.Modules.Bookmarks.Models;
using Infoboat.Modules.News.Data.DTO;
using Infoboat.Modules.News.Data.Models;
using Infoboat.Modules.News.Data.Repositories;

namespace Infoboat.Modules.News.Services
{
    public class NewsService: INewsService
    {
        private readonly INewsRepository _newsRepository;
        private readonly IBookmarkRepository _bookmarkRepository;

        public NewsService( INewsRepository newsRepository, IBookmarkRepository bookmarkRepository )
        {
            _newsRepository = newsRepository;
            _bookmarkRepository = bookmarkRepository;
        }

        public async Task<IEnumerable<NewsDTO>> GetNews( string sortBy, int pageNumber, int pageSize, string userName )
        {
            List<NewsItem> newsItems;
            switch ( sortBy )
            {
                case "latest":
                    newsItems = await _newsRepository.GetNewsPageSortedByLatest( pageNumber, pageSize );
                    break;
                default:
                    newsItems = new List<NewsItem>();
                    break;
            }

            if ( newsItems.Count == 0 ) return null;

            List<NewsDTO> newsDTOs = new List<NewsDTO>();
            IEnumerable<Bookmark> userBookmarks = await _bookmarkRepository.GetUserBookmarks( userName );

            foreach ( NewsItem newsItem in newsItems )
            {
                NewsDTO dto = new NewsDTO(newsItem);
                foreach ( Bookmark bookmark in userBookmarks )
                {
                    if ( newsItem.NewsItemId == bookmark.NewsItem.NewsItemId )
                    {
                        dto.IsBookmarked = true;
                        break;
                    }
                }
                newsDTOs.Add( dto );
            }
            return newsDTOs;
        }
    }
}