using System.Collections.Generic;
using System.Threading.Tasks;
using Infoboat.Modules.News.Data.DTO;

namespace Infoboat.Modules.News.Services
{
    public interface INewsService
    {
        Task<IEnumerable<NewsDTO>> GetNews( string sortBy, int pageNumber, int pageSize, string userName );
    }
}