using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Infoboat.Global;
using Infoboat.Modules.News.Data.Models;
using Microsoft.EntityFrameworkCore;

namespace Infoboat.Modules.News.Data.Repositories
{
    public class NewsRepository : INewsRepository
    {
        private readonly IDataContext _context;
        public NewsRepository(IDataContext context)
        {
            _context = context;
        }
        public async Task<NewsItem> GetNewsItem( int id )
        {            
            return await _context.NewsCollection.FindAsync( id );
        }

        public async Task<List<NewsItem>> GetNewsPageSortedByLatest( int pageNumber, int pageSize )
        {
            return await _context.NewsCollection.OrderByDescending( news => news.PublishedTimestamp )
                                                .Skip( ( pageNumber - 1 ) * pageSize )
                                                .Take( pageSize )
                                                .ToListAsync<NewsItem>();
        }
    }
}