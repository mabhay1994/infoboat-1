using System.Collections.Generic;
using System.Threading.Tasks;
using Infoboat.Modules.News.Data.Models;

namespace Infoboat.Modules.News.Data.Repositories
{
    public interface INewsRepository
    {
        Task<List<NewsItem>> GetNewsPageSortedByLatest(int pageNumber, int pageSize);
        Task<NewsItem> GetNewsItem(int id);
    }
}