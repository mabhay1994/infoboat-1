using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using Microsoft.EntityFrameworkCore;

namespace Infoboat.Modules.News.Data.Models
{
    [Index(nameof(Url), IsUnique = true)]
    [Index(nameof(ImgUrl), IsUnique = true)]
    [Index(nameof(Title), IsUnique = true)]
    public class NewsItem
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int NewsItemId { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public string ImgUrl { get; set; }
        public string Description { get; set; }
        public string Source { get; set; }
        public DateTime PublishedTimestamp { get; set; }
        public DateTime CreatedTimestamp { get; set; } = DateTime.Now;
    }
}