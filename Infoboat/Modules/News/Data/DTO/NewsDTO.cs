using System;
using System.Collections.Generic;
using System.Text;
using Infoboat.Modules.News.Data.Models;

namespace Infoboat.Modules.News.Data.DTO
{
    public class NewsDTO
    {
        public NewsDTO( NewsItem newsItem )
        {
            this.NewsItemId = newsItem.NewsItemId;
            this.Title = newsItem.Title;
            this.Url = newsItem.Url;
            this.ImgUrl = newsItem.ImgUrl;
            this.Description = newsItem.Description;
            this.Source = newsItem.Source;
            this.PublishedTimestamp = newsItem.PublishedTimestamp;
        }
        public int NewsItemId { get; set; }
        public string Title { get; set; }
        public string Url { get; set; }
        public string ImgUrl { get; set; }
        public string Description { get; set; }
        public string Source { get; set; }
        public DateTime PublishedTimestamp { get; set; }
        public bool IsBookmarked { get; set; } = false;
      
    }
}
