﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

namespace Infoboat.Migrations
{
    public partial class CreateCompleteDatabase : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "NewsCollection",
                columns: table => new
                {
                    NewsItemId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Title = table.Column<string>(type: "text", nullable: true),
                    Url = table.Column<string>(type: "text", nullable: true),
                    ImgUrl = table.Column<string>(type: "text", nullable: true),
                    Description = table.Column<string>(type: "text", nullable: true),
                    Source = table.Column<string>(type: "text", nullable: true),
                    PublishedTimestamp = table.Column<DateTime>(type: "timestamp without time zone", nullable: false),
                    CreatedTimestamp = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_NewsCollection", x => x.NewsItemId);
                });

            migrationBuilder.CreateTable(
                name: "Bookmarks",
                columns: table => new
                {
                    BookmarkId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    NewsItemId = table.Column<int>(type: "integer", nullable: true),
                    UserId = table.Column<string>(type: "text", nullable: true),
                    DateCreated = table.Column<DateTime>(type: "timestamp without time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Bookmarks", x => x.BookmarkId);
                    table.ForeignKey(
                        name: "FK_Bookmarks_NewsCollection_NewsItemId",
                        column: x => x.NewsItemId,
                        principalTable: "NewsCollection",
                        principalColumn: "NewsItemId",
                        onDelete: ReferentialAction.Restrict);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Bookmarks_NewsItemId",
                table: "Bookmarks",
                column: "NewsItemId");

            migrationBuilder.CreateIndex(
                name: "IX_NewsCollection_ImgUrl",
                table: "NewsCollection",
                column: "ImgUrl",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_NewsCollection_Title",
                table: "NewsCollection",
                column: "Title",
                unique: true);

            migrationBuilder.CreateIndex(
                name: "IX_NewsCollection_Url",
                table: "NewsCollection",
                column: "Url",
                unique: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Bookmarks");

            migrationBuilder.DropTable(
                name: "NewsCollection");
        }
    }
}
